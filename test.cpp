/*
 * test.cpp
 *
 *  Created on: Oct 1, 2019
 *      Author: jd11ps
 */
#include "temperature.h"
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

class TestTemp : public CppUnit::TestFixture{

	CPPUNIT_TEST_SUITE(TestTemp);
	CPPUNIT_TEST(testMix);
	CPPUNIT_TEST(testTie);
	CPPUNIT_TEST(testPositive);
	CPPUNIT_TEST(testNegative);
	CPPUNIT_TEST(testLimits);
	CPPUNIT_TEST(testEmpty);
	CPPUNIT_TEST_SUITE_END();

	float (*fn)(float*, int);

public:

	//Assign function pointer to be used by all tests
	void setUp(){
		fn = &find_zero;
	}

	void tearDown(){}

	void testMix(){

		int size = 9;
		float temps[size] = {1.123, 5.923, 80, 40, -40, -60, -2, 10000.5, -1.1234};
		float answer = 1.123;

		CPPUNIT_ASSERT(answer == fn(temps, size));
	}

	void testTie(){

			int size = 6;
			float temps[size] = {62.4, 40.123, 8.0001, -8.0001, 9.90, -12};
			float answer = 8.0001;

			CPPUNIT_ASSERT(answer == fn(temps, size));
	}

	void testPositive(){
		int size = 6;
		float temps[size] = {62.123, 40, 5.9999, 5, 9, 12.1};
		float answer = 5;

		CPPUNIT_ASSERT(answer == fn(temps, size));
	}

	void testNegative(){
		int size = 4;
		float temps[size] = {-8.2, -6.3, -1.999999, -50};
		float answer = -1.999999;

		CPPUNIT_ASSERT(answer == fn(temps, size));
	}

	void testLimits(){
		int size = 6;
		float temps[size] = {-8.2, 5526, -1.999999, -50, -273, 0.0001, -0.0001};
		float answer = 0.0001;

		CPPUNIT_ASSERT(answer == fn(temps, size));
	}

	void testEmpty(){
		int size = 0;
		float temps[size];
		float answer = 0;

		CPPUNIT_ASSERT(answer == fn(temps, size));
	}

};

CPPUNIT_TEST_SUITE_REGISTRATION(TestTemp);

int main(){

	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();

	runner.addTest(registry.makeTest());
	runner.run();

	return 0;
}
