### READ ME ###

Project uses CppUnit to unit test. This dependency has already been compiled into the executables for linux, and 32/64-bit windows.

===========

HOW TO RUN:

cd to "cppunit-example" directory, then execute the test program as follows:

Linux:

./main-linux

Windows:

32 bit: main-win-32
64 bit: main-win-64

The output will be something similar to:

......


OK (6 tests)

Indicating that all 6 test cases passed.

===========

FILE STRUCTURE:

The implementation of the temperature method is given in "temperature.h" and "temperature.cpp"

The main file for unit testing is "test.cpp"

===========

HOW TO COMPILE:

To compile and run yourself, ensure you have the CppUnit package dependency.

Linux:

sudo apt-get install libcppunit-dev

Windows:

Cygwin:

Execute your "setup-x86_64.exe" file to install the "CppUnit" package