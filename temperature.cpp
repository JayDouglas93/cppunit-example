/*
 * temperature.cpp
 *
 *  Created on: Oct 1, 2019
 *      Author: jd11ps
 */

#include <cmath>
#include "temperature.h"
#include <iostream>


float find_zero(float temps[], int size){

	if (size == 0){
		return 0;
	}

	float closest = temps[0];

	for (int i=1 ; i<size ; i++){

		float cur = temps[i];

		if (std::abs(cur) < std::abs(closest)){
			closest = cur;
		}
		else if(std::abs(cur) == std::abs(closest)){
			closest = std::abs(cur);
		}
	}

	return closest;

}
